# Tool Setup

Download the jar file

Download and install Android runtime

  - Download and [Android Studio](https://developer.android.com/studio/)

Set ANDROID_HOME

  `$ export ANDROID_HOME=<path/to/Android/Sdk>`

# Reproduce experiment(automatically)

`$ mkdir ghera-apks`

Download the apks for all 42 benchmarks from [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) and save the apks in ghera-apks

`$ cd scripts`

`$ ./runFlowdroid.sh`

# Results

See the evaluation results in *output/<config>/<benchmark>/result.xml*

# Reproduce experiment (manually)

`$ java -jar soot-infoflow-cmd-jar-with-dependencies.jar -a <apk> -p $ANDROID_HOME/platforms/ -s flowdroid/SourcesAndSinks.txt -o <output>`

# References

[Flowdroid](https://github.com/secure-software-engineering/FlowDroid)
