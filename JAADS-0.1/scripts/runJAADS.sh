export ANDROID_HOME=~/Android/Sdk
export GHERA_APKS=~/rekha-eval/ghera-apks
export OUTPUT=~/rekha-eval/JAADS/output
export JAADS_HOME=~/rekha-eval/JAADS

clean_data () {
  :
}

execute () {
  outdir=`echo $1 | grep -o '[^/]*$' | cut -d'.' -f1`
  start=`date +%s`
  timeout 15m java -jar jade-0.1.jar vulnanalysis -f $1 -p $ANDROID_HOME/platforms -c $JAADS_HOME/config -o  $OUTPUT/full/$outdir
  end=`date +%s`
  runtime=$((end-start))
  echo "$runtime s" > $OUTPUT/full/$outdir/time.txt
}

execute_fast () {
  outdir=`echo $1 | grep -o '[^/]*$' | cut -d'.' -f1`
  start=`date +%s`
  timeout 15m java -jar jade-0.1.jar vulnanalysis -f $1 -p $ANDROID_HOME/platforms -c $JAADS_HOME/config -o  $OUTPUT/fast/$outdir --fastanalysis
  end=`date +%s`
  runtime=$((end-start))
  echo "$runtime s" > $OUTPUT/fast/$outdir/time.txt
}

echo "running JAADS on Benign apks"
for b in `ls -1 $GHERA_APKS/benign/*.apk` ; do
  echo "Processing $b"
  execute $b
  execute_fast $b
  echo "done with $b"
done
echo "running JAADS on Secure apks"
for s in `ls -1 $GHERA_APKS/secure/*.apk` ; do
  echo "Processing $s"
  execute $s
  execute_fast $s
  echo "done with $s"
done
echo "done"
