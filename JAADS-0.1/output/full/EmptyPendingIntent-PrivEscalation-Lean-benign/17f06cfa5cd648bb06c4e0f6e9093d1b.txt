{
  "score": 25.0,
  "md5hash": "17f06cfa5cd648bb06c4e0f6e9093d1b",
  "results": [{
    "desc": "application is debuggable",
    "sourceStmt": "",
    "custom": "",
    "vulnKind": 3,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": ""
  }, {
    "desc": "implicit intent or receiver",
    "sourceStmt": "virtualinvoke $r4.<edu.cs.ksu.benign.MainActivity: void sendBroadcast(android.content.Intent)>($r3)",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.cs.ksu.benign.MainActivity$1: void onClick(android.view.View)>"
  }, {
    "desc": "implicit intent or receiver",
    "sourceStmt": "virtualinvoke $r0.<edu.cs.ksu.benign.MySensitiveService: void startActivity(android.content.Intent)>($r2)",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.cs.ksu.benign.MySensitiveService: int onStartCommand(android.content.Intent,int,int)>"
  }]
}