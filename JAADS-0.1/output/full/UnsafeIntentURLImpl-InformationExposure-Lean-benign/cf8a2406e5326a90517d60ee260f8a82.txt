{
  "score": 15.399999618530273,
  "md5hash": "cf8a2406e5326a90517d60ee260f8a82",
  "results": [{
    "desc": "intent parseUri",
    "sourceStmt": "$r5 = staticinvoke <android.content.Intent: android.content.Intent parseUri(java.lang.String,int)>($r2, 1)",
    "custom": "naive check, may false positive",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MyWebViewClient: boolean getUrlOverrideSettings(android.webkit.WebView,java.lang.String)>"
  }, {
    "desc": "Check webview save password disabled or not",
    "sourceStmt": "",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MainActivity: void onResume()>"
  }, {
    "desc": "application is debuggable",
    "sourceStmt": "",
    "custom": "",
    "vulnKind": 3,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": ""
  }, {
    "desc": "implicit intent or receiver",
    "sourceStmt": "virtualinvoke $r7.<android.content.Context: void startActivity(android.content.Intent)>($r5)",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MyWebViewClient: boolean getUrlOverrideSettings(android.webkit.WebView,java.lang.String)>"
  }]
}