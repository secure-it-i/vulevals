{
  "score": 6.0,
  "md5hash": "b6c59295b137a35f5dd3fd5e9979ee7b",
  "results": [{
    "desc": "implements custom verifier that always return true",
    "sourceStmt": "",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MyIntentService$2: boolean verify(java.lang.String,javax.net.ssl.SSLSession)>"
  }, {
    "desc": "X509TrustManager empty impl, lead to SSL vulnerability",
    "sourceStmt": "",
    "custom": "",
    "vulnKind": 0,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": "<edu.ksu.cs.benign.MyIntentService$1: void checkServerTrusted(java.security.cert.X509Certificate[],java.lang.String)>"
  }, {
    "desc": "application is debuggable",
    "sourceStmt": "",
    "custom": "",
    "vulnKind": 3,
    "destMethod": "",
    "paths": [],
    "destStmt": "",
    "sourceMethod": ""
  }]
}