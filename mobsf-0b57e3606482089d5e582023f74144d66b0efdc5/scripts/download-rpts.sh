export ANDROID_HOME=~/Android/Sdk
export GHERA_APKS=~/rekha-eval/ghera-apks
export OUTPUT=~/rekha-eval/mobsf/output
export MOBSF_HOME=~/rekha-eval/mobsf

clean_data () {
  :
}

download () {
  mkdir $OUTPUT/default/$2
  md5=`md5sum $1 | cut -d' ' -f1`
  curl -X POST --url http://localhost:8000/api/v1/download_pdf \
    --data "hash=$md5&scan_type=apk" \
      -H "Authorization:838b31ddd00cd7d9232bb8d735070f37a33af5dec0b8bfe7f4ef3fa65d2b3d28" > $OUTPUT/default/$2/report.pdf
}

echo "downloading reports for Benign apks"
for b in `ls -1 $GHERA_APKS/benign/*.apk` ; do
  echo "Processing $b"
  apkname=`echo $b | grep -o '[^/]*$' | cut -d'.' -f1`
  download $b $apkname
  echo "done with $b"
done
echo "downloading reports on Secure apks"
for s in `ls -1 $GHERA_APKS/secure/*.apk` ; do
  echo "Processing $s"
  apkname=`echo $s | grep -o '[^/]*$' | cut -d'.' -f1`
  download $s $apkname
  echo "done with $s"
done
echo "downloading reports on Malicious apks"
for m in `ls -1 $GHERA_APKS/malicious/*.apk` ; do
  echo "Processing $m"
  apkname=`echo $m | grep -o '[^/]*$' | cut -d'.' -f1`
  download $m $apkname
  echo "done with $m"
done
echo "done"
