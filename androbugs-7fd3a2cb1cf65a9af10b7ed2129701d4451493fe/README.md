# Tool Setup

Download the tar file

# Reproduce experiment(automatically)

`$ mkdir ghera-apks`

Download the apks for all 42 benchmarks from [Ghera](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/RekhaEval-3/) and save the apks in ghera-apks

`$ cd scripts`

`$ ./runAndrobugs.sh`

# Results

See the evaluation results in *output/<config>/<benchmark>/sha.txt*

# Reproduce experiment (manually)

`$ python AndroBugs_Framework/androbugs.py -f <apk> -o <output>`

# References

[AndroBugs](https://github.com/AndroBugs/AndroBugs_Framework)
