Application Name: HighPriority-ActivityHijack-Lean-benign.apk
Uses Permissions: 

Component edu.ksu.cs.benign.HomeActivity
  Component type: activity
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (1.onClick,L16742a)(HomeActivity.env,L34)
      Outgoing Intents via this call:
        Intent:
          Actions:
            edu.ksu.cs.benign.imageEditor
          Explicit: false
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.ImageEditor
    ICC call details are listed below:
      Caller Procedure: Landroid/support/v4/app/FragmentActivity;.startActivityForResult:(Landroid/content/Intent;I)V
      Caller Context: (2.onClick,L167480)(HomeActivity.env,L19)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.CameraActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.CameraActivity

Component edu.ksu.cs.benign.CameraActivity
  Component type: activity
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/support/v4/app/FragmentActivity;.startActivityForResult:(Landroid/content/Intent;I)V
      Caller Context: (2.onClick,L167480)(CameraActivity.env,L67)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.CameraActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.CameraActivity
    ICC call details are listed below:
      Caller Procedure: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (1.onClick,L16742a)(CameraActivity.env,L82)
      Outgoing Intents via this call:
        Intent:
          Actions:
            edu.ksu.cs.benign.imageEditor
          Explicit: false
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.ImageEditor

Component edu.ksu.cs.benign.ImageEditor
  Component type: activity
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["edu.ksu.cs.benign.imageEditor"],Categories:["android.intent.category.DEFAULT"])

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.LaunchActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (LaunchActivity.onResume,L1ea3c0)(LaunchActivity.envMain,L131)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.HomeActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.HomeActivity


Taint analysis result:
  Sources found:
  Sinks found:
    <Descriptors: api_sink: Landroid/app/Activity;.setResult:(ILandroid/content/Intent;)V 2>
    <Descriptors: api_sink: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V 1>
    <Descriptors: api_sink: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V 1>
    <Descriptors: api_sink: Landroid/app/Activity;.setResult:(ILandroid/content/Intent;)V 2>
  Discovered taint paths are listed below:
