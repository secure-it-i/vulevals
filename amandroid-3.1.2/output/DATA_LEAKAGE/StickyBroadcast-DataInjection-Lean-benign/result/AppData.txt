Application Name: StickyBroadcast-DataInjection-Lean-benign.apk
Uses Permissions: android.permission.BROADCAST_STICKY

Component edu.ksu.cs.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.sendStickyBroadcast:(Landroid/content/Intent;)V
      Caller Context: (MainActivity.onResume,L162b7e)(MainActivity.envMain,L10)
      Outgoing Intents via this call:
        Intent:
          Actions:
            santos.cs.ksu.edu.benign.MyReceiver
          Explicit: false
          Precise: true


Taint analysis result:
  Sources found:
  Sinks found:
    <Descriptors: api_sink: Landroid/content/ContextWrapper;.sendStickyBroadcast:(Landroid/content/Intent;)V 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
  Discovered taint paths are listed below:
