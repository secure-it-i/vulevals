Application Name: IncorrectHandlingImplicitIntent-UnauthorizedAccess-Lean-benign.apk
Uses Permissions: 

Component edu.ksu.cs.benign.SensitiveActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["edu.ksu.cs.benign.SENS_ACTIVITY_ACTION"],Categories:["android.intent.category.DEFAULT"])

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/support/v4/app/FragmentActivity;.startActivityForResult:(Landroid/content/Intent;I)V
      Caller Context: (1.onClick,L0f3d80)(MainActivity.envMain,L44)
      Outgoing Intents via this call:
        Intent:
          Actions:
            edu.ksu.cs.benign.SENS_ACTIVITY_ACTION
          Explicit: false
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.SensitiveActivity


Taint analysis result:
  Sources found:
    <Descriptors: callback_source: Ledu/ksu/cs/benign/MainActivity;.onActivityResult:(IILandroid/content/Intent;)V>
  Sinks found:
    <Descriptors: api_sink: Landroid/support/v4/app/FragmentActivity;.startActivityForResult:(Landroid/content/Intent;I)V 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/app/Activity;.setResult:(ILandroid/content/Intent;)V 2>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
  Discovered taint paths are listed below:
    TaintPath:
      Source: <Descriptors: callback_source: Ledu/ksu/cs/benign/MainActivity;.onActivityResult:(IILandroid/content/Intent;)V>
      Sink: <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
      Types: maliciousness:information_theft
      The path consists of the following edges ("->"). The nodes have the context information (p1 to pn means which parameter). The source is at the top :
        List(Entry@Ledu/ksu/cs/benign/MainActivity;.onActivityResult:(IILandroid/content/Intent;)V param: 3, Call@(MainActivity.onActivityResult,L147e6a)(MainActivity.envMain,L35) param: 0, Call@(MainActivity.onActivityResult,L147e6a)(MainActivity.envMain,L35), Call@(MainActivity.onActivityResult,L147e8e)(MainActivity.envMain,L35) param: 1)