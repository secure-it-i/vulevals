Application Name: BlockCipher-ECB-InformationExposure-Lean-benign.apk
Uses Permissions: 

Component edu.ksu.cs.benign.NewPasswordActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["edu.ksu.cs.benign.NEWPASS"],Categories:["android.intent.category.DEFAULT"])

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.ResetPasswordActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/app/Activity;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (2.onClick,L0f4fa4)(ResetPasswordActivity.envMain,L48)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.NewPasswordActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.NewPasswordActivity


Taint analysis result:
  Sources found:
  Sinks found:
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
  Discovered taint paths are listed below:
