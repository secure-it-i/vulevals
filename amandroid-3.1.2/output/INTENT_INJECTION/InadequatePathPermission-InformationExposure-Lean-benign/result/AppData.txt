Application Name: InadequatePathPermission-InformationExposure-Lean-benign.apk
Uses Permissions: 

Component edu.ksu.cs.benign.provider.UserDetailsContentProvider
  Component type: provider
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.UserDetailsActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["edu.ksu.cs.benign.expose.userDetails"],Categories:["android.intent.category.DEFAULT"])

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:



Taint analysis result:
  Sources found:
    <Descriptors: entrypoint_source: Ledu/ksu/cs/benign/provider/UserDetailsContentProvider;.envMain:(Landroid/content/Intent;)V>
    <Descriptors: entrypoint_source: Ledu/ksu/cs/benign/MainActivity;.envMain:(Landroid/content/Intent;)V>
    <Descriptors: entrypoint_source: Ledu/ksu/cs/benign/UserDetailsActivity;.envMain:(Landroid/content/Intent;)V>
    <Descriptors: entrypoint_source: Ledu/ksu/cs/benign/provider/UserDetailsContentProvider;.envMain:(Landroid/content/Intent;)V>
    <Descriptors: entrypoint_source: Ledu/ksu/cs/benign/MainActivity;.envMain:(Landroid/content/Intent;)V>
    <Descriptors: entrypoint_source: Ledu/ksu/cs/benign/UserDetailsActivity;.envMain:(Landroid/content/Intent;)V>
  Sinks found:
  Discovered taint paths are listed below:
