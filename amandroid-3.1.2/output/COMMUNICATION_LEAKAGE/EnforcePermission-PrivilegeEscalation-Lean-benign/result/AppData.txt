Application Name: EnforcePermission-PrivilegeEscalation-Lean-benign.apk
Uses Permissions: santos.benign.permission

Component edu.ksu.cs.benign.MyService
  Component type: service
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (MyService.onStartCommand,L10a808)(MyService.envMain,L6)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.SensitiveActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.SensitiveActivity

Component edu.ksu.cs.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startService:(Landroid/content/Intent;)Landroid/content/ComponentName;
      Caller Context: (MainActivity.onResume,L1635ac)(MainActivity.envMain,L25)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.MyService
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.MyService
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startService:(Landroid/content/Intent;)Landroid/content/ComponentName;
      Caller Context: (MainActivity.onRequestPermissionsResult,L163526)(MainActivity.envMain,L30)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.MyService
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.MyService

Component edu.ksu.cs.benign.SensitiveActivity
  Component type: activity
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startService:(Landroid/content/Intent;)Landroid/content/ComponentName;
      Caller Context: (MainActivity.onRequestPermissionsResult,L163526)(SensitiveActivity.env,L58)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.MyService
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.MyService


Taint analysis result:
  Sources found:
  Sinks found:
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
  Discovered taint paths are listed below:
