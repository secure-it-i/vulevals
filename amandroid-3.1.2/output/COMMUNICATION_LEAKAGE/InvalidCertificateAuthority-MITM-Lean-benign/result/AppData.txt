Application Name: InvalidCertificateAuthority-MITM-Lean-benign.apk
Uses Permissions: android.permission.INTERNET

Component edu.ksu.cs.benign.ResponseActivity
  Component type: activity
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:


Component edu.ksu.cs.benign.MainActivity
  Component type: activity
  Exported: true
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:
    IntentFilter:(Actions:["android.intent.action.MAIN"],Categories:["android.intent.category.LAUNCHER"])

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startService:(Landroid/content/Intent;)Landroid/content/ComponentName;
      Caller Context: (MainActivity.onResume,L1637b0)(MainActivity.envMain,L31)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.MyIntentService
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.MyIntentService

Component edu.ksu.cs.benign.MyIntentService
  Component type: service
  Exported: false
  Dynamic Registered: false
  Required Permission: 
  IntentFilters:

  Inter-component communication (ICC) Result:
    ICC call details are listed below:
      Caller Procedure: Landroid/content/ContextWrapper;.startActivity:(Landroid/content/Intent;)V
      Caller Context: (MyIntentService.onHandleIntent,L10aa5a)(MyIntentService.env,L54)
      Outgoing Intents via this call:
        Intent:
          Component Names:
            edu.ksu.cs.benign.ResponseActivity
          Explicit: true
          Precise: true
          ICC Targets:
            edu.ksu.cs.benign.ResponseActivity


Taint analysis result:
  Sources found:
    <Descriptors: api_source: Ljava/net/URLConnection;.getInputStream:()Ljava/io/InputStream;>
  Sinks found:
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Ljava/net/URL;.openConnection:()Ljava/net/URLConnection; 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 0>
    <Descriptors: api_sink: Landroid/util/Log;.d:(Ljava/lang/String;Ljava/lang/String;)I 1>
  Discovered taint paths are listed below:
